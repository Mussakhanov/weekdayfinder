//
//  ViewController.swift
//  WeekdayFinder
//
//  Created by Тимур on 25/06/2019.
//  Copyright © 2019 Timur Mussakhanov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var dateTF: UITextField!
    @IBOutlet weak var monthTF: UITextField!
    @IBOutlet weak var yearTF: UITextField!
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func findWeekDay() {
        
        let caledar = Calendar.current
        
        var dateComponents = DateComponents()//класс позволяет работать с компонентами даты
        
        guard let day = dateTF.text, let month = monthTF.text, let year = yearTF.text else { return }//развертывание что б не использовать ! принудительное извлечение
        dateComponents.day = Int(day)
        dateComponents.month = Int(month)
        dateComponents.year = Int(year)
        
        guard let date = caledar.date(from: dateComponents) else { return }
        
        let dateFormatter = DateFormatter()//класс день недели
        dateFormatter.locale = Locale(identifier: "ru_Ru")
        dateFormatter.dateFormat = "EEEE"//формат слова дня недели
        
        let weekday = dateFormatter.string(from: date)
        let capitalizedWeekday = weekday.capitalized//первая буква заглавная
        
        resultLabel.text = capitalizedWeekday

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)//убрать клавиатуру кликнув на пустое место
    }

}

